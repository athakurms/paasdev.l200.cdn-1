﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
    public class APIMController : Controller
    {
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.APIM_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.APIM_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.APIM_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.APIM_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }
        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.APIM_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string apimKey = Environment.GetEnvironmentVariable("L200_APIMKey");

            ViewBag.APIMUrl = $"https://l200{resourceGroupID}.azure-api.net/echo/resource?param1=sample";
            ViewBag.APIMKey = apimKey;
#if DEBUG
            ViewBag.APIMUrl = "https://wabac.azure-api.net/echo/resource?param1=sample";
          //  ViewBag.APIMKey = "8704ce9350c54e4788cadd1c5178c8f9";
#endif

            return View();
        }

        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.APIM_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");

            ViewBag.DocURLOne = $"https://l200{resourceGroupID}.blob.core.windows.net/content/Documentation.txt";
            ViewBag.DocURLTwo = $"https://l200{resourceGroupID}.azureedge.net/content/Documentation.txt";

#if DEBUG
            ViewBag.DocURLOne = $"https://wabacblob.blob.core.windows.net/content/Documentation.txt";
            ViewBag.DocURLTwo = $"https://wabacblob.azureedge.net/content/Documentation.txt";
#endif

            return View();
        }

        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.APIM_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");

            ViewBag.ImageOne = $"https://l200{resourceGroupID}.blob.core.windows.net/content/MS_Black.png";
            ViewBag.ImageTwo = $"https://l200{resourceGroupID}.azureedge.net/content/MS_Blue.png";

            return View();
        }

        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.APIM_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            
            string resourceGroupName = Environment.GetEnvironmentVariable("L200_ResourceGroupName");
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string subscriptionID = Environment.GetEnvironmentVariable("L200_SubscriptionID");

            ViewBag.URL = $"https://resources.azure.com/subscriptions/{subscriptionID}/resourceGroups/{resourceGroupName}/providers/Microsoft.Cdn/profiles/l200{resourceGroupID}/endpoints/l200{resourceGroupID}";


            return View();
        }
    }
}