﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PaaSDev.L200.CDN.Models;

namespace PaaSDev.L200.CDN.Controllers
{
public class CDNController : Controller
{
    private Microsoft.AspNetCore.Hosting.IHostingEnvironment _env;
    public CDNController(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
    {
        _env = env;
    }

    public IActionResult Image()
    {
        string localPath = _env.WebRootPath;
        string localFileName = "MS_Black.png";
        string sourceFile = System.IO.Path.Combine(localPath, "images", localFileName);
        return View();
    }
        private string CheckLabID(string id)
        {
            string labID = Environment.GetEnvironmentVariable("L200_LABID");

            switch (labID)
            {
                case LabModel.CDN_1:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabOne";
                    }
                case LabModel.CDN_2:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabTwo";
                    }
                case LabModel.CDN_3:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabThree";
                    }
                case LabModel.CDN_4:
                    {
                        if (string.Compare(id, labID) == 0)
                        {
                            return null;
                        }
                        return "LabFour";
                    }
                default:
                    {
                        return "LabZero";
                    }
            }
        }
        public IActionResult LabZero()
        {
            return RedirectToAction("LabZero", "Home");
        }
        public IActionResult LabOne()
        {
            var actionName = CheckLabID(LabModel.CDN_1);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            UploadFiles2StorageLabOne().Wait();

            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");

            ViewBag.ImageOne = $"https://l200{resourceGroupID}.blob.core.windows.net/content/MS_Black.png";
            ViewBag.ImageTwo = $"https://l200{resourceGroupID}.azureedge.net/content/MS_Blue.png";
            return View();
        }

        public IActionResult LabTwo()
        {
            var actionName = CheckLabID(LabModel.CDN_2);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            UploadFiles2StorageLabTwo().Wait();

            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");

            ViewBag.DocURLOne = $"https://l200{resourceGroupID}.blob.core.windows.net/content/Documentation.txt";
            ViewBag.DocURLTwo = $"https://l200{resourceGroupID}.azureedge.net/content/Documentation.txt";

#if DEBUG
            ViewBag.DocURLOne = $"https://wabacblob.blob.core.windows.net/content/Documentation.txt";
            ViewBag.DocURLTwo = $"https://wabacblob.azureedge.net/content/Documentation.txt";

            //ViewBag.DocURLTwo = $"https://l200nq6cp2ckory3e.azureedge.net/content/Documentation.txt";
#endif

            return View();
        }

        public IActionResult LabThree()
        {
            var actionName = CheckLabID(LabModel.CDN_3);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }
            UploadFiles2StorageLabOne().Wait();

            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");

            ViewBag.ImageOne = $"https://l200{resourceGroupID}.blob.core.windows.net/content/MS_Black.png";
            ViewBag.ImageTwo = $"https://l200{resourceGroupID}.azureedge.net/content/MS_Blue.png";

            return View();
        }

        public IActionResult LabFour()
        {
            var actionName = CheckLabID(LabModel.CDN_4);
            if (!string.IsNullOrEmpty(actionName))
            {
                return RedirectToAction(actionName);
            }

            UploadFiles2StorageLabOne().Wait();

            string resourceGroupName = Environment.GetEnvironmentVariable("L200_ResourceGroupName");
            string resourceGroupID = Environment.GetEnvironmentVariable("L200_ResourceGroupID");
            string subscriptionID = Environment.GetEnvironmentVariable("L200_SubscriptionID");

            ViewBag.URL = $"https://resources.azure.com/subscriptions/{subscriptionID}/resourceGroups/{resourceGroupName}/providers/Microsoft.Cdn/profiles/l200{resourceGroupID}/endpoints/l200{resourceGroupID}";


            return View();
        }

        private async Task UploadFiles2StorageLabOne()
        {
            ViewBag.Message = string.Empty;

            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;

            string sourceFile = null;
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
#if DEBUG
            storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=wabacblob;AccountKey=1QPV5UcF1ljJgWJhfc1JnRQeN3WoA3RPYEK69h3FUaRIdFjraIF2nLNL/La7Nn9t587g4yOD4chYw6V7XA7PCQ==;EndpointSuffix=core.windows.net";
#endif

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                try
                {
                    // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                    CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                    // Create a container called 'content' if doesnt exists
                    cloudBlobContainer = cloudBlobClient.GetContainerReference("content");
                    bool bNew = await cloudBlobContainer.CreateIfNotExistsAsync();

                    // this is a new container
                    if (bNew)
                    {
                        // Set the permissions so the blobs are public. 
                        BlobContainerPermissions permissions = new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Blob
                        };
                        await cloudBlobContainer.SetPermissionsAsync(permissions);
                    }

                    // List the blobs in the container.
                    BlobContinuationToken blobContinuationToken = null;
                    var results = await cloudBlobContainer.ListBlobsSegmentedAsync(null, blobContinuationToken);
                    // Get the value of the continuation token returned by the listing call.
                    //blobContinuationToken = results.ContinuationToken;

                    if (results.Results.Count() > 0)
                    {
                        return;
                    }


                    //string localPath = Microsoft.DotNet.PlatformAbstractions.Platform.ApplicationEnvironment.ApplicationBasePath;
                    string localPath = _env.WebRootPath;

                    string localFileName = "MS_Black.png";
                    //Server.MapPath(Url.Content("~/images/MS_Black.png"));
                    //HostingEnvironment.MapPath("/images/MS_Black.png")

                    sourceFile = System.IO.Path.Combine(localPath, "images", localFileName);

                    // Get a reference to the blob address, then upload the file to the blob.
                    // Use the value of localFileName for the blob name.
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName);
                    await cloudBlockBlob.UploadFromFileAsync(sourceFile);

                    localFileName = "MS_Blue.png";
                    sourceFile = System.IO.Path.Combine(localPath, "images", localFileName);
                    cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName);
                    await cloudBlockBlob.UploadFromFileAsync(sourceFile);

                    localFileName = "MS_White.jpg";
                    sourceFile = System.IO.Path.Combine(localPath, "images", localFileName);
                    cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName);
                    await cloudBlockBlob.UploadFromFileAsync(sourceFile);

                    localFileName = "MS_Store.png";
                    sourceFile = System.IO.Path.Combine(localPath, "images", localFileName);
                    cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName);
                    await cloudBlockBlob.UploadFromFileAsync(sourceFile);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = $"Error {ex.ToString()}";
                }
            }
            else
            {
                ViewBag.Message = "stroage connection string is missing.";
            }
        }

        private async Task UploadFiles2StorageLabTwo()
        {
            ViewBag.Message = string.Empty;

            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;

            string sourceFile = null;
            string storageConnectionString = Environment.GetEnvironmentVariable("L200_STORAGE");
#if DEBUG
            storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=wabacblob;AccountKey=1QPV5UcF1ljJgWJhfc1JnRQeN3WoA3RPYEK69h3FUaRIdFjraIF2nLNL/La7Nn9t587g4yOD4chYw6V7XA7PCQ==;EndpointSuffix=core.windows.net";
#endif

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                try
                {
                    // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                    CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                    // Create a container called 'content' if doesnt exists
                    cloudBlobContainer = cloudBlobClient.GetContainerReference("content");
                    bool bNew = await cloudBlobContainer.CreateIfNotExistsAsync();

                    // this is a new container
                    if (bNew)
                    {
                        // Set the permissions so the blobs are public. 
                        BlobContainerPermissions permissions = new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Blob
                        };
                        await cloudBlobContainer.SetPermissionsAsync(permissions);
                    }

                    // List the blobs in the container.
                    BlobContinuationToken blobContinuationToken = null;
                    var results = await cloudBlobContainer.ListBlobsSegmentedAsync(null, blobContinuationToken);
                    // Get the value of the continuation token returned by the listing call.
                    //blobContinuationToken = results.ContinuationToken;

                    if (results.Results.Count() > 0)
                    {
                        return;
                    }


                    string localPath = _env.WebRootPath;

                    string localFileName = "Documentation.txt";
                    
                    sourceFile = System.IO.Path.Combine(localPath, "content", localFileName);

                    // Get a reference to the blob address, then upload the file to the blob.
                    // Use the value of localFileName for the blob name.
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName);
                    await cloudBlockBlob.UploadFromFileAsync(sourceFile);

                   
                }
                catch (Exception ex)
                {
                    ViewBag.Message = $"Error {ex.ToString()}";
                }
            }
            else
            {
                ViewBag.Message = "stroage connection string is missing.";
            }
        }
        public IActionResult Index()
        {
            var actionName = CheckLabID(string.Empty);
            return RedirectToAction(actionName);
        }
    }
}