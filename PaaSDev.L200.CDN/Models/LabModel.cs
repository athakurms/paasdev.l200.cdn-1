﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSDev.L200.CDN.Models
{
    public class LabModel
    {
        public const string CDN_1 = "CDN_1";
        public const string CDN_2 = "CDN_2";
        public const string CDN_3 = "CDN_3";
        public const string CDN_4 = "CDN_4";

        public const string APIM_1 = "APIM_1";
        public const string APIM_2 = "APIM_2";
        public const string APIM_3 = "APIM_3";
        public const string APIM_4 = "APIM_4";
    }
}
